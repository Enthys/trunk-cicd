- Creating releases from feature, bug or hotfix branches is unnecessary
- CHANGELOG.md file is unnecessary. The releases match the changes in the same
way and do not add complexity to the deployment process.
